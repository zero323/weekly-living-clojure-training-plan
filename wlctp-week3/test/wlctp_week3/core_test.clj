(ns wlctp-week3.core-test
  (:require [clojure.test :refer :all]
            [wlctp-week3.core :refer :all]))

(deftest week3-test
    (testing "Test #43"
    (is (= (reverse-interleave [1 2 3 4 5 6] 2) '((1 3 5) (2 4 6))))
    (is (= (reverse-interleave (range 9) 3) '((0 3 6) (1 4 7) (2 5 8))))
    (is (= (reverse-interleave (range 10) 5) '((0 5) (1 6) (2 7) (3 8) (4 9)))))
  (testing "Test #44"
    (is (= (rotate-seq 2 [1 2 3 4 5]) '(3 4 5 1 2)))
    (is (= (rotate-seq -2 [1 2 3 4 5]) '(4 5 1 2 3)))
    (is (= (rotate-seq 6 [1 2 3 4 5]) '(2 3 4 5 1)))
    (is (= (rotate-seq 1 '(:a :b :c)) '(:b :c :a)))
    (is (= (rotate-seq -4 '(:a :b :c)) '(:c :a :b))))
  (testing "Test #46"
    (is (= 3 ((flip-out nth) 2 [1 2 3 4 5])))
    (is (= true ((flip-out >) 7 8)))
    (is (= 4 ((flip-out quot) 2 8)))
    (is (= [1 2 3] ((flip-out take) [1 2 3 4 5] 3))))
  (testing "Test #50"
    (is (= (set (split-by-type [1 :a 2 :b 3 :c])) #{[1 2 3] [:a :b :c]}))
    (is (= (set (split-by-type [:a "foo" "bar" :b])) #{[:a :b] ["foo" "bar"]}))
    (is (= (set (split-by-type [[1 2] :a [3 4] 5 6 :b])) #{[[1 2] [3 4]] [:a :b] [5 6]})))
  (testing "Test #77"
    (is (= (find-anagrams ["meat" "mat" "team" "mate" "eat"]) #{#{"meat" "team" "mate"}}))
    (is (= (find-anagrams ["veer" "lake" "item" "kale" "mite" "ever"]) #{#{"veer" "ever"} #{"lake" "kale"} #{"mite" "item"}})))
  (testing "Test #95"
    (is (= (is-tree? '(:a (:b nil nil) nil)) true))
    (is (= (is-tree? '(:a (:b nil nil))) false))
    (is (= (is-tree? [1 nil [2 [3 nil nil] [4 nil nil]]]) true))
    (is (= (is-tree? [1 [2 nil nil] [3 nil nil] [4 nil nil]]) false))
    (is (= (is-tree? [1 [2 [3 [4 nil nil] nil] nil] nil]) true))
    (is (= (is-tree? [1 [2 [3 [4 false nil] nil] nil] nil]) false))
    (is (= (is-tree? '(:a nil ())) false)))
  (testing "Test #96"
    (is (= (is-symmetric? '(:a (:b nil nil) (:b nil nil))) true))
    (is (= (is-symmetric? '(:a (:b nil nil) nil)) false))
    (is (= (is-symmetric? '(:a (:b nil nil) (:c nil nil))) false))
    (is (= (is-symmetric? [1 [2 nil [3 [4 [5 nil nil] [6 nil nil]] nil]] [2 [3 nil [4 [6 nil nil] [5 nil nil]]] nil]]) true))
    (is (= (is-symmetric? [1 [2 nil [3 [4 [5 nil nil] [6 nil nil]] nil]] [2 [3 nil [4 [5 nil nil] [6 nil nil]]] nil]]) false))
    (is (= (is-symmetric? [1 [2 nil [3 [4 [5 nil nil] [6 nil nil]] nil]] [2 [3 nil [4 [6 nil nil] nil]] nil]]) false)))
)
