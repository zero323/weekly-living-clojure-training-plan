(ns wlctp-week3.core)

;; https://www.4clojure.com/problem/43
(defn reverse-interleave [x n]
  (apply (partial map list) (partition n x)))

;; https://www.4clojure.com/problem/44
(defn rotate-seq [n coll]
  (let [m (mod n (count coll))]
    (concat (drop m coll) (take m coll))))

;; https://www.4clojure.com/problem/46
(defn flip-out [f] (fn [x y] (f y x)))

;; https://www.4clojure.com/problem/50
(defn split-by-type [x] (set (vals (group-by type x))))

;; https://www.4clojure.com/problem/77
(defn find-anagrams [x]
  (reduce-kv
   (fn [acc k v] (if (> (count v) 1) (conj acc (set v)) acc))
   #{}
   (group-by #(group-by identity %) x)))

;; https://www.4clojure.com/problem/95
(defn is-tree? [x]
  (every?
   #(or (and (sequential? %) (= (count %) 3)) (nil? %))
   (tree-seq sequential? rest x)))

;; https://www.4clojure.com/problem/96
(defn is-symmetric? [[e l r]]
  (letfn[(mirror [node] (if (nil? node) nil (let [[e l r] node] [e (mirror r) (mirror l)])))]
         (= l (mirror r))))
