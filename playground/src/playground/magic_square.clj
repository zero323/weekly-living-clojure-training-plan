(ns playground.magic-square
  (:refer-clojure :exclude [==])
  (:require [clojure.core.logic :refer :all]
            [clojure.core.logic.fd :as fd]
            [clojure.core.logic.pldb :as pldb]
            ))

 (defn magic-square []
   (run 1 [q]
    (fresh [a b c d e f g h i x]
      (fd/in a b c d e f g h i (fd/interval 1 10))
      (fd/in x (fd/interval 6 27))
      (fd/eq
        (= (+ a b c) x)
        (= (+ d e f) x)
        (= (+ g h i) x)
        (= (+ a d g) x)
        (= (+ b e h) x)
        (= (+ c f i) x)
        (= (+ a e i) x)
        (= (+ c e g) x))
      (fd/distinct [a b c d e f g h i])
      (== q [[a b c] [d e f] [g h i]]))))


 (magic-square)
