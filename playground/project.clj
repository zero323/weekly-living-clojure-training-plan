(defproject playground "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                [org.clojure/core.logic "0.8.10"]
                [cheshire "5.5.0"]
                [clj-yaml "0.4.0"]
                [clj-http "1.1.2"]])
