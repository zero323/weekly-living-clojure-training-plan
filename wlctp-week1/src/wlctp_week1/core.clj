(ns wlctp-week1.core
   (:require [clojure.set]))

;; https://4clojure.com/problem/1
(= true true)

;; https://4clojure.com/problem/2
(= (- 10 (* 2 3)) 6)

;; https://www.4clojure.com/problem/3
(= "HELLO WORLD" (.toUpperCase "hello world"))

;; https://www.4clojure.com/problem/4
(= (list :a :b :c) '(:a :b :c))

;; https://www.4clojure.com/problem/5
(= '(1 2 3 4) (conj '(2 3 4) 1))
(= '(1 2 3 4) (conj '(3 4) 2 1))

;; https://www.4clojure.com/problem/6
(= [:a :b :c] (list :a :b :c) (vec '(:a :b :c)) (vector :a :b :c))

;; https://www.4clojure.com/problem/7
(= [1 2 3 4] (conj [1 2 3] 4))
(= [1 2 3 4] (conj [1 2] 3 4))

;; https://www.4clojure.com/problem/8
(= #{:a :b :c :d} (set '(:a :a :b :c :c :c :c :d :d)))
(= #{:a :b :c :d} (clojure.set/union #{:a :b :c} #{:b :c :d}))

;; https://www.4clojure.com/problem/9
(= #{1 2 3 4} (conj #{1 4 3} 2))

;; https://www.4clojure.com/problem/10
(= 20 ((hash-map :a 10, :b 20, :c 30) :b))
(= 20 (:b {:a 10, :b 20, :c 30}))

;; https://www.4clojure.com/problem/11
(= {:a 1, :b 2, :c 3} (conj {:a 1} [:b 2] [:c 3]))

;; https://www.4clojure.com/problem/12
(= 3 (first '(3 2 1)))
(= 3 (second [2 3 4]))
(= 3 (last (list 1 2 3)))

;; https://www.4clojure.com/problem/13
(= [20 30 40] (rest [10 20 30 40]))

;; https://www.4clojure.com/problem/14
(= 8 ((fn add-five [x] (+ x 5)) 3))
(= 8 ((fn [x] (+ x 5)) 3))
(= 8 (#(+ % 5) 3))
(= 8 ((partial + 5) 3))

;; https://www.4clojure.com/problem/15
(defn times-two [x] (* x 2))

;; https://www.4clojure.com/problem/16
(defn hello-you [name] (str "Hello, " name "!"))

;; https://www.4clojure.com/problem/17
(= '(6 7 8) (map #(+ % 5) '(1 2 3)))

;; https://www.4clojure.com/problem/18
(= '(6 7) (filter #(> % 5) '(3 4 5 6 7)))

;; https://www.4clojure.com/problem/19
(defn last-one [x] (first (reverse x)))

;; https://www.4clojure.com/problem/20
(defn second-to-last [x] (second (reverse x)))

;; https://www.4clojure.com/problem/24
(defn sum [x] (apply + x))

;; https://www.4clojure.com/problem/25
(defn filter-odd [x] (filter odd? x))

;; https://www.4clojure.com/problem/27
(defn is-palindrome [x] (= (seq x) (reverse x)))

;; https://www.4clojure.com/problem/30
(defn compress [s] (map first (partition-by identity s)))

;; https://www.4clojure.com/problem/31
(defn pack-seq [s] (partition-by identity s))

;; https://www.4clojure.com/problem/32
(defn duplicate [x] (interleave x x))

;; https://www.4clojure.com/problem/33
(defn my-replicate [s n] (apply concat (map #(repeat n %1) s)))

;; https://www.4clojure.com/problem/41
(defn drop-nth [s n]
  (for [[x y] (map list s (cycle (range 1 (inc n)))) :when (not= y n)] x))

(defn drop-nth [s n] (keep-indexed #(if (not= (rem (inc %1) n) 0) %2) s))

;; https://www.4clojure.com/problem/45
(= '(1 4 7 10 13) (take 5 (iterate #(+ 3 %) 1)))

;; https://www.4clojure.com/problem/35
(= 7 (let [x 5] (+ 2 x)))
(= 7 (let [x 3, y 10] (- y x)))
(= 7 (let [x 21] (let [y 3] (/ x y))))

;; https://www.4clojure.com/problem/36
(= 10 (let [x 7 y 3 z 1] (+ x y)))
(= 4 (let [x 7 y 3 z 1] (+ y z)))
(= 1 (let [x 7 y 3 z 1] z))

;; https://www.4clojure.com/problem/37
(= "ABC" (apply str (re-seq #"[A-Z]+" "bA1B3Ce ")))

;; https://www.4clojure.com/problem/57
(= '(5 4 3 2 1) ((fn foo [x] (when (> x 0) (conj (foo (dec x)) x))) 5))

;; https://www.4clojure.com/problem/68
(= '(7 6 5 4 3)
  (loop [x 5
         result []]
    (if (> x 0)
      (recur (dec x) (conj result (+ 2 x)))
      result)))

;; https://www.4clojure.com/problem/71
(= (last (sort (rest (reverse [2 5 4 1 3 6]))))
   (-> [2 5 4 1 3 6] (reverse) (rest) (sort) (last))
   5)

;; https://www.4clojure.com/problem/72
(= (reduce + (map inc (take 3 (drop 2 [2 5 4 1 3 6]))))
   (->> [2 5 4 1 3 6] (drop 2) (take 3) (map inc) (reduce +))
   11)

;; https://www.4clojure.com/problem/145
(= (range 1 40 4) (for [x (range 40)
            :when (= 1 (rem x 4))]
        x))

(= (range 1 40 4) (for [x (iterate #(+ 4 %) 0)
            :let [z (inc x)]
            :while (< z 40)]
        z))

(= (range 1 40 4) (for [[x y] (partition 2 (range 20))]
        (+ x y)))
