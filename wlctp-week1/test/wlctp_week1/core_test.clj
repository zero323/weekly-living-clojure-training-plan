(ns wlctp-week1.core-test
  (:require [clojure.test :refer :all]
            [wlctp-week1.core :refer :all]))

(deftest week1-test
  (testing "Test #15"
    (is (= (times-two 2) 4))
    (is (= (times-two 3) 6))
    (is (= (times-two 11) 22))
    (is (= (times-two 7) 14)))
  (testing "Test #16"
    (is (= (hello-you "Dave") "Hello, Dave!"))
    (is (= (hello-you "Jenn") "Hello, Jenn!"))
    (is (= (hello-you "Rhea") "Hello, Rhea!")))
  (testing "Test #24"
    (is (= (sum [1 2 3]) 6))
    (is (= (sum (list 0 -2 5 5)) 8))
    (is (= (sum #{4 2 1}) 7))
    (is (= (sum '(0 0 -1)) -1))
    (is (= (sum '(1 10 3)) 14)))
  (testing "Test #25"
    (is (= (filter-odd #{1 2 3 4 5}) '(1 3 5)))
    (is (= (filter-odd [4 2 1 6]) '(1)))
    (is (= (filter-odd [2 2 4 6]) '()))
    (is (= (filter-odd [1 1 1 3]) '(1 1 1 3))))
  (testing "Test #27"
    (is (false? (is-palindrome '(1 2 3 4 5))))
    (is (true? (is-palindrome "racecar")))
    (is (true? (is-palindrome [:foo :bar :foo])))
    (is (true? (is-palindrome '(1 1 3 3 1 1))))
    (is (false? (is-palindrome '(:a :b :c)))))
  (testing "Test #30"
    (is (= (apply str (compress "Leeeeeerrroyyy")) "Leroy"))
    (is (= (compress [1 1 2 3 3 2 2 3]) '(1 2 3 2 3)))
    (is (= (compress [[1 2] [1 2] [3 4] [1 2]]) '([1 2] [3 4] [1 2]))))
  (testing "Test #31"
    (is (= (pack-seq [1 1 2 1 1 1 3 3]) '((1 1) (2) (1 1 1) (3 3))))
    (is (= (pack-seq [:a :a :b :b :c]) '((:a :a) (:b :b) (:c))))
    (is (= (pack-seq [[1 2] [1 2] [3 4]]) '(([1 2] [1 2]) ([3 4])))))
  (testing "Test #32"
    (is (= (duplicate [1 2 3]) '(1 1 2 2 3 3)))
    (is (= (duplicate [:a :a :b :b]) '(:a :a :a :a :b :b :b :b)))
    (is (= (duplicate [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4])))
    (is (= (duplicate [[1 2] [3 4]]) '([1 2] [1 2] [3 4] [3 4]))))
  (testing "Test #33"
    (is (= (my-replicate [1 2 3] 2) '(1 1 2 2 3 3)))
    (is (= (my-replicate [:a :b] 4) '(:a :a :a :a :b :b :b :b)))
    (is (= (my-replicate [4 5 6] 1) '(4 5 6)))
    (is (= (my-replicate [[1 2] [3 4]] 2) '([1 2] [1 2] [3 4] [3 4]))))
  (testing "Test #41"
    (is (= (drop-nth [1 2 3 4 5 6 7 8] 3) [1 2 4 5 7 8]))
    (is (= (drop-nth [:a :b :c :d :e :f] 2) [:a :c :e]))
    (is (= (drop-nth [1 2 3 4 5 6] 4) [1 2 3 5 6])))
  )
