(ns fox-goose-bag-of-corn.puzzle
  (:require [clojure.set :refer :all]))

(def start-pos [[[:fox :goose :corn :you] [:boat] []]])

(defn- solution? [state]
  ;; Check if current sate is a solution
  (= {:left #{} :boat #{:boat} :right #{:you :fox :goose :corn}} state))

(defn- neighbors [pos]
  ({:left [:boat] :right [:boat] :boat [:left :right]} pos))

(def ^{:private true} not-movable #{:boat})

(defn- pre-map [states]
  (for [[left boat right] states]
    [{:left (set left) :boat (set boat) :right (set right)}]))

(defn- post-map [states]
  (for [{:keys [left boat right]} states]
    [left boat right]))

(defn- safe? [state]
  ;; Check if current state is valid
  (let [invalid #{#{:boat :fox} #{:boat :goose} #{:boat :corn} #{:fox :goose} #{:goose :corn} #{:fox :goose :corn}}]
    (empty? (filter #(contains? invalid %) (vals state)))))

(defn- move [source-name sink-names state]
  ;; Create a list of possible moves from source to sinks
  ;; Extract source
  (let [source (state source-name)]
    ;; For each animal (:you is animal as well) and sink
    (for [animal (difference source not-movable) sink-name sink-names]
      ;; Create a set of moved items, new source and sink states
      (let [moved (set [:you animal]) new-source (difference source moved) new-sink (union (state sink-name) moved)]
        ;; Updated state
        (merge state {source-name new-source sink-name new-sink})))))

(defn- moves [state]
  ;; Find source (position of :you)
  (let [source (first (for [[k v] state :when (contains? v :you)] k))]
    ;; Find possible moves
    (filter safe? (move source (neighbors source) state))))

(defn- extend-path [moves path visited]
  ;; If new state not in visited
  (map #(cons % path) (filter #(not (contains? visited %)) moves)))

(defn river-crossing-plan []
  (loop [q (pre-map start-pos) visited #{}]
    ;; Get a first path from q and extract last state
    (let [path (first q) state (first path)]
      (if (solution? state)
        (reverse (post-map path))
        ;; Update q and visited
        (recur (concat (rest q) (extend-path (moves state) path visited)) (conj visited state))))))







