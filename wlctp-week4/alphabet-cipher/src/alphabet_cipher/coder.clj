(ns alphabet-cipher.coder)

(defn ^{:private true} alphabet [min max]
  {:pre [(> max min)]}
  (mapv char (range min (inc max))))

(defn  ^{:private true} int-pos [alphabet]
  (fn [x] (- (int x) (int (first alphabet)))))

(defn ^{:private true} code-mapper [alphabet shift]
  (let [nchar (count alphabet) int-pos (int-pos alphabet)]
    (fn [x y] (alphabet (mod (+ (shift (int-pos x)) (int-pos y)) nchar)))))

(defn ^{:private true} gencoder [alphabet shift]
  (fn [keyword message]
    (apply str (map (code-mapper alphabet shift) (cycle keyword) message))))

(def ^{:private true} az-alphabet (alphabet 97 122))

(def encode (gencoder az-alphabet +))
(def decode (gencoder az-alphabet -))
