(ns wonderland-number.finder)

(defn wonderland-number []
  (let [candidates (range (int 1e5) (int 1e6))
        digits (fn [x] (set (str x)))
        is-valid? (fn [x i] (= (digits x) (digits (* x i))))]
    (first (filter #(every? identity (map (partial is-valid? %) (range 2 7))) candidates))))
