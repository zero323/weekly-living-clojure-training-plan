(ns card-game-war.game)

;; feel free to use these cards or use your own data structure
(def suits [:spade :club :diamond :heart])
(def ranks [2 3 4 5 6 7 8 9 10 :jack :queen :king :ace])
(def cards
  (for [suit suits
        rank ranks]
    [suit rank]))


(def card-values
  (let [ranks-values (zipmap ranks (iterate inc 1)) suit-values (zipmap suits (iterate inc 1))]
    (into {} (for [[suit rank :as card] cards] [card (+ (* (ranks-values rank) 100) (suit-values suit))]))))

(defn play-round [player1-card player2-card]
  (if (> (card-values player1-card) (card-values player2-card))
    [[player1-card player2-card] []]
    [[] [player2-card player1-card]]))


(defn play-game [player1-cards player2-cards]
  (loop [player1-cards player1-cards player2-cards player2-cards]
    (if (empty? player1-cards) :player2
      (if (empty? player2-cards) :player1
        (let
          [[player1-result player2-result] (play-round (first player1-cards) (first player2-cards))]
          (recur
           (concat (rest player1-cards) player1-result)
           (concat (rest player2-cards) player2-result)))))))
