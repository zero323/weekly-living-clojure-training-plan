(ns card-game-war.game-test
  (:require [clojure.test :refer :all]
            [card-game-war.game :refer :all]))


;; fill in  tests for your game
(deftest test-play-round
  (testing "the highest rank wins the cards in the round"
    (is (= (play-round [:spade :ace] [:spade 2]) [[[:spade :ace] [:spade 2]] []])))
  (testing "queens are higher rank than jacks"
    (is (= (play-round [:spade :queen] [:spade :jack]) [[[:spade :queen] [:spade :jack]] []])))
  (testing "kings are higher rank than queens"
    (is (= (play-round [:heart :queen] [:heart :king]) [[] [[:heart :king] [:heart :queen]]])))
  (testing "aces are higher rank than kings"
    (is (= (play-round [:club :ace] [:club :king]) [[[:club :ace] [:club :king]] []])))
  (testing "if the ranks are equal, clubs beat spades"
    (is (= (play-round [:club 2] [:spade 2]) [[[:club 2] [:spade 2]] []])))
  (testing "if the ranks are equal, diamonds beat clubs"
    (is (= (play-round [:club :ace] [:diamond :ace]) [[] [[:diamond :ace] [:club :ace]]])))
  (testing "if the ranks are equal, hearts beat diamonds")
    (is (= (play-round [:heart 10] [:diamond 10]) [[[:heart 10] [:diamond 10]] []])))

(deftest test-play-game
  (testing "the player loses when they run out of cards"
    (is (= :player1 (play-game cards [])))
    (is (= :player2 (play-game [] cards)))))

