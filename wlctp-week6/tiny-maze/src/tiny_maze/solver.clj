(ns tiny-maze.solver
  (:require [clojure.math.numeric-tower :as math]))

(defn- south [pos](let [[i j] pos] [(inc i) j]))
(defn- north [pos] (let [[i j] pos] [(dec i) j]))
(defn- west [pos] (let [[i j] pos] [i (dec j)]))
(defn- east [pos] (let [[i j] pos] [i (inc j)]))
(defn- dim [maze] [(count maze) (count (first maze))])

(defn- neighbors [valid pos]
  (filter (partial contains? valid) (map #(%1 pos) [north east south west])))

(defn- find-pos [maze pos]
  (let [[nrow ncol] (dim maze)]
    (for [i (range nrow) j (range ncol) :when (= (get-in maze [i j]) pos)] [i j])))

(defn- find-start [maze]
  (first (find-pos maze :S)))

(defn- find-end [maze]
  (first (find-pos maze :E)))

(defn- find-valid [maze]
  (conj (set (find-pos maze 0)) (find-end maze)))

(defn- dist [pos1 pos2]
  (+ (math/abs (- (first pos2) (first pos1))) (math/abs (- (last pos2) (last pos1)))))

(defn- extend-path [path visited valid]
  (let [last-pos (first path)]
    (map #(conj path %) (filter #(not (contains? visited %)) (neighbors valid last-pos)))))

(defn post-map [maze solution]
  (let [[nrow ncol] (dim maze)]
    (for [i (range nrow)]
      (for [j (range ncol)] (if (contains? solution [i j]) :x (get-in maze [i j]))))))


(defn solve-maze [maze]
  (let [start-pos (find-start maze) end-pos (find-end maze) valid (find-valid maze)]
    (loop [paths [(list start-pos)] visited #{}]
      (if (empty? paths) nil
        (let [path (first paths) last-pos (first path)]
          (if (= last-pos end-pos)
            (post-map maze (set path))
            (recur (concat (rest paths) (extend-path path visited valid)) (conj visited last-pos))))))))


