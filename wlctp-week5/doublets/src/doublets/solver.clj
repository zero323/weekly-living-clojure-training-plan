(ns doublets.solver
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]))

(def words (-> "words.edn"
               (io/resource)
               (slurp)
               (read-string)))

(defn- dist [word1 word2]
  ;; Count number of different characters
  {:pre [(= (count word1) (count word2))]}
  (reduce + (map #(if (= %1 %2) 0 1) word1 word2)))

(defn- build-dict [word1 words]
  (filter #(= (count %) (count word1)) words))

(defn- neighbors [word dict]
  ;; A neighbor is a word with exactly one mismatch
  ;; No insertions/deletions allowed
  (filter #(= 1 (dist word %)) dict))

(def ^{:private true} not-contains?
  (complement contains?))

(defn- extend-path [path dict visited]
  ;; Take path and generate list of extended paths using neighbors
  (let [head (first path)]
    (->>
     (neighbors head dict)
     (filter (partial not-contains? visited))
     (map (partial conj path)))))

(defn- doublets-loop [paths target dict]
  (loop [paths paths visited #{}]
    ;; If paths empty return empty list
    (if (empty? paths) []
      (let [path (first paths) head (first path)]
        ;; If target reached return reversed path
        (if (= head target) (reverse path)
          (recur
           ;; Extend current path and add to paths
           (concat (extend-path path dict visited) (rest paths))
           ;; Add head to visited
           (conj visited head)))))))

(defn doublets [word1 word2]
  (if (= (count word1) (count word2))
    (doublets-loop [(list word1)] word2 (build-dict word1 words)) []))
