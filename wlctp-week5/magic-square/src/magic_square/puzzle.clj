(ns magic-square.puzzle
  (:require [clojure.math.combinatorics :refer [permutations]]
            [clojure.math.numeric-tower :refer [sqrt]]))

(defn- rows [values n] (partition n values))
(defn- cols [values n] (apply (partial map list) (rows values n)))
(defn- neg-diag [values n] (map #(nth %2 (- n 1 %1)) (range n) (partition n values)))
(defn- pos-diag [values n] (map #(nth %2 %1) (range n) (partition n values)))
(defn- diags [values n] [(pos-diag values n) (neg-diag values n)])
(defn- sums [values] (map (partial apply +) values))
(defn- sums-equal? [values] (apply = (sums values)))
(defn- rows-equal? [values n] (sums-equal? (rows values n)))
(defn- cols-equal? [values n] (sums-equal? (cols values n)))
(defn- diags-equal? [values n] (sums-equal? (diags values n)))
(defn- is-magic? [values n] (sums-equal? (concat (rows values n) (cols values n) (diags values n))))

(def values [1.0 1.5 2.0 2.5 3.0 3.5 4.0 4.5 5.0])

(defn magic-square [values]
  (let [n (sqrt (count values))]
  (mapv
   #(apply vector %)
   (rows
   (first
    (filter
     #(and (rows-equal? % n) (cols-equal? % n) (diags-equal? % n) (is-magic? % n))
    (permutations values))) n))))
