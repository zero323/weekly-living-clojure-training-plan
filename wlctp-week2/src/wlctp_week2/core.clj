(ns wlctp-week2.core
   (:require [clojure.set]))

;; https://www.4clojure.com/problem/26
(defn fibonacci-seq-iter [n]
  (take n (map first (iterate #(let [[x y] %1] [y (+ x y)]) [1 1]))))

(defn fibonacci-seq-rec [n]
  (loop [x '(1 0) i 1]
    (if (= i n)
      (reverse (butlast x))
      (recur (conj x (+ (first x) (second x))) (inc i)))))

;; https://www.4clojure.com/problem/29
(defn keep-caps [s] (clojure.string/replace s #"[^A-Z]" ""))

;; https://www.4clojure.com/problem/42
(defn factorial [n] (reduce * 1 (range 1 (inc n))))

;; Lesson URL: https://www.4clojure.com/problem/48
(= 6 (some #{2 7 6} [5 6 7 8]))
(= 6 (some #(when (even? %) %) [5 6 7 8]))

;; https://www.4clojure.com/problem/51
(= [1 2 [3 4 5] [1 2 3 4 5]] (let [[a b & c :as d] (range 1 6)] [a b c d]))

;; https://www.4clojure.com/problem/52
(= [2 4] (let [[a b c d e f g] (range)] [c e]))

;; https://www.4clojure.com/problem/66
(defn gcd [x y]
  (loop [x x y y]
    (if (zero? y) x (recur y (mod x y)))))

;; https://www.4clojure.com/problem/83
(defn half-truth [& x]
  (= (set x) #{true false}))

;; https://www.4clojure.com/problem/88
(defn symmetric-difference [x y]
    (clojure.set/difference
     (clojure.set/union x y)
     (clojure.set/intersection x y)))

;; https://www.4clojure.com/problem/90
(defn cartesian-product [xs ys]
  (set (for [x xs y ys] [x y])))

;; https://www.4clojure.com/problem/97
(defn pascal-row [n]
  (loop [x [1] n n]
    (if (= n 1)
      x
      (recur (map + (cons 0 x) (concat x [0])) (dec n)))))

;; https://www.4clojure.com/problem/100
(defn lcm [x & z]
  (let [
    gcd (fn [x y]
      (loop [x x y y]
        (if (zero? y) x (recur y (mod x y)))))
    lcm2 (fn [x y] (/ (* x y) (gcd x y)))]
  (reduce lcm2 (cons x z))))

;; https://www.4clojure.com/problem/107
(defn pow [n] {:pre [(not (neg? n))]}
  (partial (fn [n x] (if (zero? n) 1 (reduce * (repeat n x)))) n))
