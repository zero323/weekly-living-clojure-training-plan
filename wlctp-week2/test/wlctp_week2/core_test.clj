(ns wlctp-week2.core-test
  (:require [clojure.test :refer :all]
            [wlctp-week2.core :refer :all]))

(deftest week2-test
  (testing "Test #25"
    (is (= (fibonacci-seq-iter 3) '(1 1 2)))
    (is (= (fibonacci-seq-iter 6) '(1 1 2 3 5 8)))
    (is (= (fibonacci-seq-iter 8) '(1 1 2 3 5 8 13 21)))
    (is (= (fibonacci-seq-rec 3) '(1 1 2)))
    (is (= (fibonacci-seq-rec 6) '(1 1 2 3 5 8)))
    (is (= (fibonacci-seq-rec 8) '(1 1 2 3 5 8 13 21))))
  (testing "Test #29"
    (is (= (keep-caps "HeLlO, WoRlD!") "HLOWRD"))
    (is (empty? (keep-caps "nothing")))
    (is (= (keep-caps "$#A(*&987Zf") "AZ")))
  (testing "Test #42"
    (is (= (factorial 1) 1))
    (is (= (factorial 3) 6))
    (is (= (factorial 5) 120))
    (is (= (factorial 8) 40320)))
  (testing "Test #65"
    (is (= (gcd 2 4) 2))
    (is (= (gcd 10 5) 5))
    (is (= (gcd 5 7) 1))
    (is (= (gcd 1023 858) 33)))
  (testing "Test #83"
    (is (= false (half-truth false false)))
    (is (= true (half-truth true false)))
    (is (= false (half-truth true)))
    (is (= true (half-truth false true false)))
    (is (= false (half-truth true true true)))
    (is (= true (half-truth true true true false))))
  (testing "Test #88"
    (is (= (symmetric-difference #{1 2 3 4 5 6} #{1 3 5 7}) #{2 4 6 7}))
    (is (= (symmetric-difference #{:a :b :c} #{}) #{:a :b :c}))
    (is (= (symmetric-difference #{} #{4 5 6}) #{4 5 6}))
    (is (= (symmetric-difference #{[1 2] [2 3]} #{[2 3] [3 4]}) #{[1 2] [3 4]})))
  (testing "Test #90"
    (is (= (cartesian-product #{"ace" "king" "queen"} #{"♠" "♥" "♦" "♣"})
     #{["ace"   "♠"] ["ace"   "♥"] ["ace"   "♦"] ["ace"   "♣"]
     ["king"  "♠"] ["king"  "♥"] ["king"  "♦"] ["king"  "♣"]
     ["queen" "♠"] ["queen" "♥"] ["queen" "♦"] ["queen" "♣"]}))
    (is (= (cartesian-product #{1 2 3} #{4 5}) #{[1 4] [2 4] [3 4] [1 5] [2 5] [3 5]}))
    (is (= 300 (count (cartesian-product (into #{} (range 10)) (into #{} (range 30)))))))
  (testing "Test #97"
    (is (= (pascal-row 1) [1]))
    (is (= (map pascal-row (range 1 6))
      [
      [1]
      [1 1]
      [1 2 1]
      [1 3 3 1]
      [1 4 6 4 1]]))
    (is (= (pascal-row 11)
      [1 10 45 120 210 252 210 120 45 10 1])))
  (testing "Test #100"
    (is (== (lcm 2 3) 6))
    (is (== (lcm 5 3 7) 105))
    (is (== (lcm 1/3 2/5) 2))
    (is (== (lcm 3/4 1/6) 3/2))
    (is (== (lcm 7 5/7 2 3/5) 210)))
  (testing "Test #107"
    (is (= 256 ((pow 2) 16) ((pow 8) 2)))
    (is (= [1 8 27 64] (map (pow 3) [1 2 3 4])))
    (is (= [1 2 4 8 16] (map #((pow %) 2) [0 1 2 3 4]))))
  )
